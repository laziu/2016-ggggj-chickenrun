﻿using UnityEngine;
using System.Collections;

public class CameraController: MonoBehaviour {
    
    #region Manager
    public static CameraController Inst { get; private set; }

    void Awake() {
        Inst = this;
    }

    void OnDestroy() {
        if (Inst == this)
            Inst = null;
    }
    #endregion

    [SerializeField]
    Camera[] cameras;

    int _camera = 0;
    public int Camera {
        get {
            return _camera;
        }
        set {
            if (_camera != value) {
                cameras[_camera].gameObject.SetActive(false);
                cameras[value].gameObject.SetActive(true);
                _camera = value;
            }
        }
    }

	void Start () {
        for (int i = 0; i < cameras.Length; ++i)
            cameras[i].gameObject.SetActive(false);
        cameras[0].gameObject.SetActive(true);
	}
}
