﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStartButton: MonoBehaviour {
    public void StartGameScene() {
        SceneManager.LoadScene("GameScene");
    }
}
