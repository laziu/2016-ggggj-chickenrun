﻿using UnityEngine;
using System.Collections;

public class GameFlow: MonoBehaviour {

    #region Manager
    public static GameFlow Inst { get; private set; }

    void Awake() {
        Inst = this;
    }

    void OnDestroy() {
        if (Inst == this)
            Inst = null;
    }
    #endregion

    public GameObject[] chickens = new GameObject[6];
    public Animator[] _animator = new Animator[6]; 
    float[] speed = new float[6];
    int[] position = new int[6];
    bool[] skill = new bool[6];
    public float[] rate = new float[6];

    float raceTime = 0;

	void Start () {
	    foreach (var i in chickens) {
            var v = i.transform.position;
            v.x = 0;
            i.transform.position = v;
        }
        StartCoroutine(Ready());
	}
	
	void Update () {
        raceTime += Time.deltaTime;
        for (int i = 0; i < chickens.Length; ++i) {
            chickens[i].transform.position += Vector3.right * (speed[i] + (skill[i] ? 3.0f : 0)) * Time.deltaTime;
            if (cameraState == 0 && chickens[i].transform.position.x > 160f) {
                cameraState = 1;
                StartCoroutine(CameraChange(1));
            }
            else if (cameraState == 1 && chickens[i].transform.position.x > 400f) {
                cameraState = 2;
                CameraController.Inst.Camera = 2;
            }
            else if (cameraState == 2 && chickens[i].transform.position.x > 600f) {
                cameraState = 3;
                CameraController.Inst.Camera = 0;
            }
            else if (cameraState == 3 && chickens[i].transform.position.x > 800f) {
                cameraState = 4;
                StartCoroutine(CameraChange(3));
            }

            int state = (int)(chickens[i].transform.position.x / 100f);
            if (state != position[i]) {
                position[i] = state;
                CheckSkill(i);
            }
            if (chickens[i].transform.position.x > 1000f && rate[i] < 0.1f) {
                rate[i] = raceTime;
            }
        }
    }

    public void CheckSkill(int index) {
        if (Random.Range(0.0f, 1.0f) < 0.5f) {
            skill[index] = true;
            _animator[index].SetBool("Skill", true);
        }
        else {
            skill[index] = false;
            _animator[index].SetBool("Skill", false);
        }
    }

    public int cameraState = 0;
    IEnumerator CameraChange(int index) {
        CameraController.Inst.Camera = index;
        yield return new WaitForSeconds(3f);
        CameraController.Inst.Camera = 0;
    }
    
    IEnumerator Ready() {
        yield return new WaitForSeconds(3f);
        GameUI.Inst.Shadow(true);

        GameUI.Inst.Message("3");
        yield return new WaitForSeconds(1f);

        GameUI.Inst.Message("2");
        yield return new WaitForSeconds(1f);

        GameUI.Inst.Message("1");
        yield return new WaitForSeconds(1f);

        GameUI.Inst.Message("경기 시작");
        GameUI.Inst.Shadow(false);
        StartCoroutine(Gaming());
    }

    IEnumerator Gaming() {
        raceTime = 0;
        StartCoroutine(CheckGameEnd());

        while (true) {
            bool finished = true;
            foreach (var i in chickens) {
                if (i.transform.position.x < 1000f) {
                    finished = false;
                    break;
                }
            }

            if (finished)
                break;

            for (int i = 0; i < speed.Length; ++i)
                speed[i] = 15 + Random.Range(-1f, 1f);
            yield return new WaitForSeconds(5f);
        }
    }

    IEnumerator CheckGameEnd() {
        while (true) {
            bool finished = true;
            foreach (var i in chickens) {
                if (i.transform.position.x < 1000f) {
                    finished = false;
                    break;
                }
            }
            if (finished)
                break;
            yield return new WaitForSeconds(0.5f);
        }
        StartCoroutine(End());
    }

    IEnumerator End() {
        GameUI.Inst.Shadow(true);
        GameUI.Inst.End();
        yield return null;
    }
}
