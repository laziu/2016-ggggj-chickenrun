﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameUI: MonoBehaviour {

    #region Manager
    public static GameUI Inst { get; private set; }

    void Awake() {
        Inst = this;
    }

    void OnDestroy() {
        if (Inst == this)
            Inst = null;
    }
    #endregion

    [SerializeField]
    Image _shadowPanel;
    [SerializeField]
    Text _messageText;

    [SerializeField]
    Text[] _raceTimeResultText;
    [SerializeField]
    Image[] _rankResultImage;
    [SerializeField]
    Sprite[] _chickenSprite;

    [SerializeField]
    Canvas _normalCanvas;
    [SerializeField]
    Canvas _resultCanvas;

    bool _shadowEnable = false;

	void Start () {
	
	}
	
	void Update () {
        Color c = _shadowPanel.color;
        c.a = _shadowEnable ? c.a * 0.7f + 0.4f * 0.3f : c.a * 0.7f;
        _shadowPanel.color = c;
    }

    public void Shadow(bool enable) {
        _shadowEnable = enable;
    }

    public void Message(string msg) {
        StartCoroutine(_MessageCoroutine(msg));
    }

    IEnumerator _MessageCoroutine(string msg) {
        _messageText.enabled = true;
        _messageText.text = msg;
        Vector3 idv = new Vector3(1, 1, 1);
        for (float time = 0.0f; time < 0.9f; time += Time.deltaTime) {
            if (time < 0.2f) {
                Color c = _messageText.color;
                c.a = time / 0.2f;
                _messageText.color = c;
                _messageText.transform.localScale = idv * (1.1f + (0.2f - time) * 3f);
            }
            else if (time < 0.7f) {
                _messageText.transform.localScale = idv * (0.9f + (0.7f - time) * 0.4f);
            }
            else {
                Color c = _messageText.color;
                c.a = c.a * 0.7f;
                _messageText.color = c;
            }
            yield return null;
        }
        _messageText.enabled = false;
    }

    public void End() {
        _normalCanvas.gameObject.SetActive(false);
        _resultCanvas.gameObject.SetActive(true);
        int length = GameFlow.Inst.chickens.Length;

        int[] rank = new int[length];
        for (int i = 0; i < length; ++i)
            rank[i] = i;
        for (int i = 0; i < length; ++i)
            for (int j = i + 1; j < length; ++j)
                if (GameFlow.Inst.rate[rank[i]] > GameFlow.Inst.rate[rank[j]]) {
                    int t = rank[i];
                    rank[i] = rank[j];
                    rank[j] = t;
                }

        for (int i = 0; i < length; ++i) {
            _raceTimeResultText[i].text = GameFlow.Inst.rate[rank[i]].ToString();
            _rankResultImage[i].sprite = _chickenSprite[rank[i]];
        }
    }

    public void RefreshGame() {
        Debug.Log("Reload " + SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
