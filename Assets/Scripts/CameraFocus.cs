﻿using UnityEngine;
using System.Collections;

public class CameraFocus: MonoBehaviour {

    [SerializeField]
    Vector3 offset;
    [SerializeField]
    Vector3 rotation;
    Quaternion rotQuat;

    bool ended = false;

    void Start () {
        rotQuat = Quaternion.Euler(rotation);
        transform.SetParent(GameFlow.Inst.chickens[0].transform);
	}
	
	void Update () {
        if (!ended) {
            Vector3 pos = transform.parent.position + offset;
            if (pos.x < 0) {
                pos.x = 0;
                transform.position = Vector3.Lerp(transform.position, pos, 0.1f);
            }
            else if (pos.x > 1000f) {
                transform.SetParent(null, true);
                transform.position = Vector3.Lerp(transform.position,
                    new Vector3(1000f, transform.position.y, transform.position.z), 0.1f);
                ended = true;
            }
            else {
                transform.localPosition = Vector3.Lerp(
                    transform.localPosition, offset, 0.1f);
            }
            transform.localRotation = Quaternion.Lerp(
                transform.localRotation, rotQuat, 0.1f);
        }
        else {
            transform.position = Vector3.Lerp(transform.position,
                new Vector3(1000f, transform.position.y + 20f * Time.deltaTime, transform.position.z - 100f * Time.deltaTime), 0.1f);
        }
    }

    public void FocusToTarget(int index) {
        if (!ended)
            transform.SetParent(GameFlow.Inst.chickens[index].transform);
    }
}
